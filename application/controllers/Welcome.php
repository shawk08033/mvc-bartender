<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function index()
	{
		$data['drinks'] = $this->Drink_model->get_drinks();
		
		$this->load->view('home', $data);
	}
	
	public function addDrink($drink)
	{
		$data = array('Name' => urldecode($drink));
		$this->db->insert('que', $data);
		
		$data['drinks'] = $this->Drink_model->get_drinks();
		$data['message'] = "Ordered 1 " . urldecode($drink);
		$this->load->view('home', $data);
	}
	
	public function Manager_View()
	{
		$data['drinks'] = $this->Drink_model->get_que();	
		$this->load->view('view_orders', $data);
	}
	
	public function delete_order($id)
	{
		$this->db->delete('que', array('id' => $id)); 
		$data['drinks'] = $this->Drink_model->get_que();	
		$this->load->view('view_orders', $data);	
	}
}
