<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>BarTender</title>
</head>
<body>

<center><h1>Order Drinks</h1></center>
<div style="color:red;">
<?php
if(isset($message))
{
	echo $message;
}
?>
</div>
</br>
<center>
<?php foreach($drinks as $drink): ?>
<h3><?php echo $drink['Name'];?></h3>
</br>
<?php $CI =& get_instance();?>
$<?php echo $drink['Price'];?> &nbsp; <a href="<?php echo base_url();?>/welcome/addDrink/<?php echo $drink['Name']?>">Order</a></br>
<?php echo $drink['Description']; ?>
<?php endforeach;?>
</center>
</br>
<a href="<?php echo base_url();?>/welcome/Manager_View">View Orders</a>
</body>
</html>