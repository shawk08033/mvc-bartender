<?php
	class Drink_model extends CI_Model{
		public function __construct(){
			$this->load->database();
		}
		
		public function get_drinks(){
			$query = $this->db->get('drinks');
			return $query->result_array();
		}
		
		public function get_que(){
			$query = $this->db->get('que');
			return $query->result_array();
		}
	}
?>